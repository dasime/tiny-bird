# tiny-bird

A theme for the [Hugo](https://gohugo.io/) framework.

## Redirect Config

The redirect code is based on [Thomas Jensen](https://blog.cavelab.dev/)'s
[blog post](https://blog.cavelab.dev/2021/01/hugo-aliases-to-nginx-map-file/)
([Internet Archive](https://web.archive.org/web/20211120083135/https://blog.cavelab.dev/2021/01/hugo-aliases-to-nginx-map-file/)).

When migrating to Hugo, I changed domain and completely redid the path.

To preserve the legacy URLs, I recorded them in the YAML frontmatter on those
posts and pages like this:

```yaml
---
title: Foo post
aliases:
  - /2014/08/foo-post.html # old example.com location
---
```

Then during build, a custom `outputFormat` configured in `config.toml`
and defined at `layouts/index.redir.txt` is used as the template to
build a plaintext map for usage in Nginx.

```toml
[outputs]
  home = ["HTML", "REDIR"]

[outputFormats.REDIR]
  baseName = "redirects"
  isPlainText = true
  mediaType = "text/plain"
  notAlternative = true
```

This file gets pushed to my webserver and the following is an example
of how it's configured in Nginx.

```nginx
map_hash_bucket_size 256; # see http://nginx.org/en/docs/hash.html

map $request_uri $new_uri {
    include /srv/www/example.com/index.redir.txt;
}

server {
    # If there is a valid redirect for the request, 301.
    # If there isn't a valid redirect for the request, 410.
    listen      80;
    server_name example.com www.example.com;
    root        /srv/www/example.com;

    if ($new_uri) {
      return 301 https://example.org$new_uri;
    }

    location / {
      add_header Cache-Control "public, max-age=3600";
    }

    return 410;
}
```

The logic here:

1.  A request comes in for `example.com`
2.  If the request was for a legacy page or article, return a
    `301 Moved Permanently` to the new path on `example.org`
3.  Return `410 Gone` for all other requests
